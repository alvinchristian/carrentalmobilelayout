import {StyleSheet, View, Dimensions, StatusBar, Image} from 'react-native';
import React, {useEffect} from 'react';
import {StackActions} from '@react-navigation/native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.dispatch(StackActions.replace('App'));
    }, 2000);
  });
  return (
    <View style={styles.Container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Image
        style={styles.Image}
        source={require('../assets/Splash.png')}></Image>
    </View>
  );
};

const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#091B6F',
    justifyContent: 'flex-end',
  },
  Image: {
    width: screen.width * 1,
    height: screen.height * 0.88,
  },
});

export default Splash;
