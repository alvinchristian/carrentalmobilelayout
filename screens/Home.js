import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import React from 'react';
import List from '../components/List';
import ButtonMenu from '../components/ButtonMenu';

const Home = () => {
  return (
    <ScrollView style={{backgroundColor: '#fff'}}>
      <View style={styles.Container}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="dark-content"
        />
        <View style={styles.Tone}></View>
        <View style={styles.Content}>
          <View style={styles.Header}>
            <View>
              <Text style={styles.Name}>Hi, Alvin</Text>
              <Text style={styles.Location}>Palu, Sulawesi Tengah</Text>
            </View>
            <Image
              style={styles.Profile}
              source={require('../assets/Alvin.jpg')}
            />
          </View>

          <Image
            style={styles.Banner}
            source={require('../assets/Banner.png')}
          />

          <View style={styles.ButtonMenus}>
            <ButtonMenu Name="truck" Caption="Sewa Mobil" />
            <ButtonMenu Name="box" Caption="Oleh-Oleh" />
            <ButtonMenu Name="key" Caption="Penginapan" />
            <ButtonMenu Name="camera" Caption="Wisata" />
          </View>

          <View style={styles.List}>
            <Text style={styles.ListTitle}> Daftar Mobil Pilihan</Text>
            <List />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  Container: {
    alignItems: 'center',
  },
  Tone: {
    position: 'absolute',
    backgroundColor: '#D3D9FD',
    width: screen.width * 1,
    height: 176,
  },
  Content: {
    marginTop: 46,
  },
  Header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  Name: {
    fontSize: 12,
    color: '#000',
    marginBottom: 4,
  },
  Location: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000',
  },
  Profile: {
    width: 28,
    height: 28,
    borderRadius: 28,
  },
  Banner: {
    marginTop: 16,
    width: screen.width * 0.91,
    height: screen.width * 0.39,
  },
  ButtonMenus: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 32,
  },
  List: {
    marginTop: 24,
  },
  ListTitle: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000',
    marginBottom: 16,
  },
});

export default Home;
