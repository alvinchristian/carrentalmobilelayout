import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import React from 'react';

const Akun = () => {
  return (
    <View style={styles.Container}>
      <Text style={styles.Title}>Akun</Text>
      <View style={styles.Content}>
        <Image style={styles.Image} source={require('../assets/Allura.png')} />
        <Text style={styles.Text}>
          Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <TouchableOpacity style={styles.Button}>
          <Text style={styles.ButtonText}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Title: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000',
    marginTop: 32,
    paddingVertical: 18,
    paddingHorizontal: 16,
  },
  Content: {
    height: screen.height * 0.75,
    alignItems: 'center',
    paddingHorizontal: 25,
    justifyContent: 'center',
  },
  Image: {
    width: 312,
    height: 192,
  },
  Text: {
    fontSize: 14,
    marginVertical: 16,
    lineHeight: 20,
    color: '#000',
    textAlign: 'center',
  },
  Button: {
    backgroundColor: '#5CB85F',
    height: 36,
    width: 81,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
  },
  ButtonText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default Akun;
