import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const data = [
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
  {
    Image: require('../assets/Mercedes.png'),
    Name: 'Daihatsu Xenia',
    Users: 4,
    BriefCase: 2,
    Price: 'Rp 230.000',
  },
];

const List = () => {
  return (
    <View>
      {data.map((item, i) => (
        <View key={i}>
          <TouchableOpacity style={styles.ItemContainer}>
            <Image style={styles.Image} source={item.Image}></Image>
            <View>
              <Text style={styles.Name}>{item.Name}</Text>
              <View style={styles.Icons}>
                <Icon name="users" size={12}></Icon>
                <Text style={styles.Value}>{item.Users}</Text>
                <Icon name="briefcase" style={styles.Icon} size={12}></Icon>
                <Text style={styles.Value}>{item.BriefCase}</Text>
              </View>
              <Text style={styles.Price}>{item.Price}</Text>
            </View>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
};

const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  ItemContainer: {
    width: screen.width * 0.91,
    height: 98,
    flexDirection: 'row',
    padding: 16,
    backgroundColor: '#fff',
    marginBottom: 16,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2,

    elevation: 5,
  },
  Image: {
    marginRight: 16,
    marginVertical: 8,
  },
  Name: {
    color: '#000',
    fontSize: 14,
    marginBottom: 4,
  },
  Icons: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },
  Icon: {
    marginLeft: 10,
  },
  Value: {
    color: '#8A8A8A',
    fontSize: 10,
    marginHorizontal: 4,
  },
  Price: {
    color: '#5CB85F',
    fontSize: 14,
  },
});

export default List;
