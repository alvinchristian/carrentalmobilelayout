import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './screens/Home';
import DaftarMobil from './screens/DaftarMobil';
import Akun from './screens/Akun';
import Splash from './screens/Splash';

const BottomTabsSize = () => {
  if (Platform.OS === 'ios') {
    return 90;
  } else {
    return 60;
  }
};

const App = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarIcon: ({size, color}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Daftar Mobil') {
            iconName = 'list';
          } else if (route.name === 'Akun') {
            iconName = 'user';
          }
          return <Icon name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#0D28A6',
        tabBarInactiveTintColor: '#222222',
        tabBarStyle: {
          height: BottomTabsSize(),
        },
        tabBarItemStyle: {
          height: 40,
          alignSelf: 'center',
        },
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Daftar Mobil" component={DaftarMobil} />
      <Tab.Screen name="Akun" component={Akun} />
    </Tab.Navigator>
  );
};

const Stack = createNativeStackNavigator();
const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="App"
          component={App}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Route;
